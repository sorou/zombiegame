package com.gitlab.sorou.foodsurvivalgame;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.gitlab.sorou.foodsurvivalgame.bullet.AmmoType;
import com.gitlab.sorou.foodsurvivalgame.bullet.Banana;
import com.gitlab.sorou.foodsurvivalgame.bullet.Bullet;
import com.gitlab.sorou.foodsurvivalgame.bullet.Gumball;
import com.gitlab.sorou.foodsurvivalgame.bullet.Watermelon;

import static com.gitlab.sorou.foodsurvivalgame.Direction.randomDir;

public class Enemy extends Person {
    public float shotTimer;
    private float dirChangeTimer;
    public AmmoType ammoType = AmmoType.randomAmmoType();
    private AssetManager assetManager;
    private Sound fireSound;
    private Sound explodeSound;

    public Enemy(Vector2 initialPos, AssetManager assetManager) {
        this.assetManager = assetManager;
        fireSound = assetManager.get("sounds/shoot.wav");
        explodeSound = assetManager.get("sounds/boom.wav");

        pos = initialPos;
        velocity = new Vector2(0f, 0f);
        collisionRect = new Rectangle(0f, 0f, GameParams.PERSON_SIZE, GameParams.PERSON_SIZE);
        dir = randomDir();
        // random initially so that all humans don't change direction in unison
        dirChangeTimer = MathUtils.random(GameParams.ENEMY_SECONDS_PER_DIR_CHANGE);

        super.speed = GameParams.ENEMY_SPEED;
        loadAnimations(assetManager);
        initShotTimer();
    }

    @Override
    protected void loadAnimations(AssetManager assetManager) {
        TextureAtlas atlas;
        switch (ammoType) {
            case GUMBALL:
                atlas = assetManager.get("enemies/gmachine/gumball_machine.atlas");
                super.leftWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("gmachine_left"), Animation.PlayMode.LOOP);
                super.rightWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("gmachine_right"), Animation.PlayMode.LOOP);
                super.upWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("gmachine_back"), Animation.PlayMode.LOOP);
                super.downWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("gmachine"), Animation.PlayMode.LOOP);
                super.downAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("gmachine_atk"));
                animsToFinish.add(downAttackAnim);
                super.leftAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("gmachine_left_atk"));
                animsToFinish.add(leftAttackAnim);
                super.rightAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("gmachine_right_atk"));
                animsToFinish.add(rightAttackAnim);
                super.setCurrentAnimation(downWalkAnim);
                break;
            case BANANA:
                atlas = assetManager.get("enemies/bbunch/banana_bunch.atlas");
                super.leftWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("banana_bunch_walk_left"), Animation.PlayMode.LOOP);
                super.rightWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("banana_bunch_walk_right"), Animation.PlayMode.LOOP);
                super.upWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("banana_bunch_walk_up"), Animation.PlayMode.LOOP);
                super.downWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("banana_bunch_walk_down"), Animation.PlayMode.LOOP);
                super.downAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("banana_bunch_down_atk"));
                animsToFinish.add(downAttackAnim);
                super.leftAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("banana_bunch_left_atk"));
                animsToFinish.add(leftAttackAnim);
                super.rightAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("banana_bunch_right_atk"));
                animsToFinish.add(rightAttackAnim);
                super.setCurrentAnimation(downWalkAnim);
                break;
            case WATERMELON:
                atlas = assetManager.get("enemies/watermelon/watermelon.atlas");
                super.leftWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("watermelon_walk_left"), Animation.PlayMode.LOOP);
                super.rightWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("watermelon_walk_right"), Animation.PlayMode.LOOP);
                super.upWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("watermelon_walk_up"), Animation.PlayMode.LOOP);
                super.downWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("watermelon_walk_down"), Animation.PlayMode.LOOP);
                super.downAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("watermelon_down_atk"));
                animsToFinish.add(downAttackAnim);
                super.leftAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("watermelon_left_atk"));
                animsToFinish.add(leftAttackAnim);
                super.rightAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("watermelon_right_atk"));
                animsToFinish.add(rightAttackAnim);
                super.setCurrentAnimation(downWalkAnim);
                break;
            default:
                throw new IllegalArgumentException("We shouldn't have gotten here.");
        }
    }
    @Override
    public void update(float delta) {

        super.animationTimer += delta;
        if (super.getCurrentAnimation().isAnimationFinished(animationTimer)) {
            move(dir);
        }

        dirChangeTimer -= delta;
        if (dirChangeTimer <= delta) {
            dir = randomDir();
            // TODO enemies spawn at the edge so they're more likely to stay there
            // weighted random in favor of moving inwards towards the screen?
            dirChangeTimer = MathUtils.random(GameParams.ENEMY_SECONDS_PER_DIR_CHANGE);
        }

        shotTimer -= delta;
        Vector2 scaledVelocity = velocity.scl(delta);
        setPos(super.pos.x + scaledVelocity.x, pos.y + scaledVelocity.y);

        move(dir);
    }

    public Bullet shoot(Person target) {
        // calling cpy() here is important so that we don't screw with the shooter's position
        // by spawning a bullet
        Vector2 tgtUnitVector = target.pos.cpy().sub(this.pos).limit(1);
        switch(dir) {
            case DOWN:
                setCurrentAnimation(downAttackAnim);
                break;
            case LEFT:
                setCurrentAnimation(leftAttackAnim);
                break;
            case RIGHT:
                setCurrentAnimation(rightAttackAnim);
                break;
            default:
                break;
        }

        switch (ammoType) {
            // send bullet in same direction as target with speed adjusted
            // we can get a vector in the same direction as another vector by getting a unit vector
            // and scaling it to the desired magnitude
            case GUMBALL:
                fireSound.play();
                shotTimer = GameParams.GUMBALL_SECONDS_PER_SHOT;
                return new Gumball(this.pos, tgtUnitVector.scl(GameParams.GUMBALL_SPEED), assetManager);
            case BANANA:
                fireSound.play();
                shotTimer = GameParams.BANANA_SECONDS_PER_SHOT;
                return new Banana(this, tgtUnitVector.scl(GameParams.BANANA_SPEED), assetManager);
            case WATERMELON:
                explodeSound.play();
                shotTimer = GameParams.WATERMELON_SECONDS_PER_SHOT;
                return new Watermelon(this.pos, assetManager);
            default:
                throw new IllegalStateException("We shouldn't have gotten here");

        }
    }

    private void initShotTimer() {
        switch (ammoType) {
            case GUMBALL:
                this.shotTimer = MathUtils.random(GameParams.GUMBALL_SECONDS_PER_SHOT);
                break;
            case BANANA:
                this.shotTimer = MathUtils.random(GameParams.BANANA_SECONDS_PER_SHOT);
                break;
            case WATERMELON:
                this.shotTimer = MathUtils.random(GameParams.WATERMELON_SECONDS_PER_SHOT);
                break;
            default:
                throw new IllegalStateException("We shouldn't have gotten here.");

        }
    }

    @Override
    public void stop() {
        explodeSound.stop();
        fireSound.stop();
        super.stop();
    }
}
