package com.gitlab.sorou.foodsurvivalgame;

public class GameParams {
    public static float GAME_WIDTH = 640;
    public static float GAME_HEIGHT = 480;

    public static float PERSON_SIZE = 32;

    public static float PLAYER_SPEED = 300;

    public static int POINTS_PER_ENEMY = 10;

    public static float ENEMY_SECONDS_PER_DIR_CHANGE = 1;
    public static float ENEMY_SPEED = 100;
    public static float ENEMY_SPAWN_SECONDS = 2;

    public static float GUMBALL_RADIUS = 16;
    public static int GUMBALL_DAMAGE = 10;
    public static float GUMBALL_SPEED = 300;
    public static float GUMBALL_SECONDS_PER_SHOT = 1f;

    public static float BANANA_RADIUS = 16;
    public static int BANANA_DAMAGE = 15;
    public static float BANANA_RANGE = 300;
    public static float BANANA_SPEED = 150;
    public static float BANANA_ACCEL = 300;
    public static float BANANA_SECONDS_PER_SHOT = 2f;

    public static float WATERMELON_RADIUS = 100;
    public static int WATERMELON_DAMAGE = 25;
    public static float WATERMELON_SPEED = 0;
    public static float WATERMELON_SECONDS_PER_SHOT = 3;
    public static float WATERMELON_EXPLOSION_DURATION_SEC = 0.5f;

    public static float SECONDS_PER_LOOPING_ANIM_FRAME = 0.25f;
    public static float SECONDS_PER_NORMAL_ANIM_FRAME = 0.0625f;

    public static float HEALTH_BAR_WIDTH = 100f;
    public static float HEALTH_BAR_HEIGHT = 16f;
}
