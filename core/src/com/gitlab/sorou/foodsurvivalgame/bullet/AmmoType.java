package com.gitlab.sorou.foodsurvivalgame.bullet;

import com.badlogic.gdx.math.MathUtils;

public enum AmmoType {
    GUMBALL,
    BANANA,
    WATERMELON;

    public static AmmoType randomAmmoType() {
        switch (MathUtils.random(2)) {
            case 0:
                return AmmoType.GUMBALL;
            case 1:
                return AmmoType.BANANA;
            default:
                return AmmoType.WATERMELON;
        }
    }
}
