package com.gitlab.sorou.foodsurvivalgame.bullet;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.gitlab.sorou.foodsurvivalgame.GameParams;
import com.gitlab.sorou.foodsurvivalgame.Person;

public class Banana extends Bullet {
    float distanceTraveled = 0f;
    public Person source;
    public boolean isReturning = false;

    public Banana(Person source, Vector2 initVelocity, AssetManager assetManager) {
        this(source.getPos(), initVelocity, assetManager);
        this.source = source;
    }

    public Banana(Vector2 initPos, Vector2 initVelocity, AssetManager assetManager) {
        super(initPos, initVelocity, assetManager);
        super.collisionCircle.radius = GameParams.BANANA_RADIUS;
        super.damage = GameParams.BANANA_DAMAGE;
    }

    @Override
    protected void loadAnimations() {
        TextureAtlas atlas = assetManager.get("objects/banana.atlas");
        anim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("banana"), Animation.PlayMode.LOOP);
    }

    @Override
    public void update(float delta) {
        Vector2 previousPos = super.pos.cpy();
        super.update(delta);

        if (!isReturning) {
            distanceTraveled += previousPos.dst(super.pos);
            if (distanceTraveled > GameParams.BANANA_RANGE) {
                isReturning = true;
            }
        } else {
            // we don't do any further acceleration if there's nothing to return to
            if (!source.isActive) {
                return;
            }
            // accelerate towards whatever threw it
            Vector2 newTargetUnitVector = source.getPos().cpy().sub(this.pos).limit(1);
            Vector2 acceleration = newTargetUnitVector.scl(GameParams.BANANA_ACCEL);
            super.velocity = super.velocity.add(acceleration.scl(delta));
            float speed = super.velocity.len();
            // todo this is probably expensive, see if we can make it happen any less often
            if (speed > GameParams.BANANA_SPEED) {
                super.velocity.set(super.velocity.cpy().limit(1).scl(GameParams.BANANA_SPEED));
            }
        }
    }

    @Override
    public void drawDebug(ShapeRenderer shapeRenderer) {
        Color origColor = new Color(shapeRenderer.getColor());
        shapeRenderer.setColor(Color.YELLOW);
        super.drawDebug(shapeRenderer);
        shapeRenderer.setColor(origColor);
    }
}
