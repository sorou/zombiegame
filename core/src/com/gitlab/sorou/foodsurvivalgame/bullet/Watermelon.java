package com.gitlab.sorou.foodsurvivalgame.bullet;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.gitlab.sorou.foodsurvivalgame.GameParams;

public class Watermelon extends Bullet {

    private float explosionTimer = GameParams.WATERMELON_EXPLOSION_DURATION_SEC;

    public boolean explosionFinished = false;

    public Watermelon(Vector2 initPos, AssetManager assetManager) {
        super(initPos, Vector2.Zero, assetManager);
        super.collisionCircle.radius = GameParams.WATERMELON_RADIUS;
        super.damage = GameParams.WATERMELON_DAMAGE;
    }

    @Override
    protected void loadAnimations() {
        TextureAtlas atlas = assetManager.get("objects/watermelon_explosion.atlas");
        anim = new Animation<>(Long.MAX_VALUE, atlas.findRegions("watermelon_explosion"));
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        explosionTimer -= delta;
        explosionFinished = explosionTimer <= 0;
    }

    @Override
    public void drawDebug(ShapeRenderer shapeRenderer) {
        Color origColor = new Color(shapeRenderer.getColor());
        shapeRenderer.setColor(Color.GREEN);
        super.drawDebug(shapeRenderer);
        shapeRenderer.setColor(origColor);
    }
}
