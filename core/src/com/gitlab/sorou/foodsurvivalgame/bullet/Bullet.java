package com.gitlab.sorou.foodsurvivalgame.bullet;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public abstract class Bullet {
    public float damage;
    public Circle collisionCircle;
    protected Vector2 pos;
    protected Vector2 velocity;

    protected AssetManager assetManager;
    protected Animation<TextureRegion> anim;
    protected float animationTimer = 0f;
    protected boolean isStopped = false;

    public Bullet(Vector2 initPos, Vector2 initVelocity, AssetManager assetManager) {
        this.assetManager = assetManager;
        this.pos = new Vector2(initPos);
        this.velocity = new Vector2(initVelocity);
        // this radius will be overridden by subclasses
        // probably not the best way to do it but this is a one-shot throwaway simple game
        this.collisionCircle = new Circle(pos.x, pos.y, 0f);
        loadAnimations();
    }

    protected abstract void loadAnimations();

    public void update(float delta) {
        if (!isStopped) {
            animationTimer += delta;
            this.pos.add(velocity.cpy().scl(delta));
            this.collisionCircle.x = this.pos.x;
            this.collisionCircle.y = this.pos.y;
        }
    }

    public void draw(SpriteBatch batch) {
        if (!isStopped) {
            // the position is x, y starting from lower left corner of texture
            // but the position is the center of the collision circle
            batch.draw(anim.getKeyFrame(animationTimer),
                    pos.x - collisionCircle.radius, pos.y - collisionCircle.radius,
                    collisionCircle.radius*2, collisionCircle.radius*2);
        }
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        if (!isStopped) {
            shapeRenderer.circle(collisionCircle.x, collisionCircle.y,
                    collisionCircle.radius);
        }
    }

    public void stop() {
        isStopped = true;
    }
}
