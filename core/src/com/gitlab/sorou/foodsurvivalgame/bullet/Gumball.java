package com.gitlab.sorou.foodsurvivalgame.bullet;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.gitlab.sorou.foodsurvivalgame.GameParams;

public class Gumball extends Bullet {

    public Gumball(Vector2 initPos, Vector2 initVelocity, AssetManager assetManager) {
        super(initPos, initVelocity, assetManager);
        super.collisionCircle.radius = GameParams.GUMBALL_RADIUS;
        super.damage = GameParams.GUMBALL_DAMAGE;
    }

    @Override
    protected void loadAnimations() {
        // yes, gumball only has a single frame of animation. sue me.
        TextureAtlas atlas = assetManager.get("objects/gumball.atlas");
        anim = new Animation<>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("gumball"), Animation.PlayMode.LOOP);
    }

    @Override
    public void drawDebug(ShapeRenderer shapeRenderer) {
        Color origColor = new Color(shapeRenderer.getColor());
        shapeRenderer.setColor(Color.RED);
        super.drawDebug(shapeRenderer);
        shapeRenderer.setColor(origColor);
    }

}
