package com.gitlab.sorou.foodsurvivalgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.gitlab.sorou.foodsurvivalgame.screen.StartScreen;

public class FoodSurvivalGame extends Game {
    public final AssetManager assetManager = new AssetManager();

    @Override
    public void create() {
        this.setScreen(new StartScreen(this));
    }
}
