package com.gitlab.sorou.foodsurvivalgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Player extends Person {

    public int health = 100;
    private Animation<TextureRegion> deathAnimation;
    private Animation<TextureRegion> hitUp;
    private Animation<TextureRegion> hitDown;
    private Animation<TextureRegion> hitRight;
    private Animation<TextureRegion> hitLeft;
    private Sound hurtSound;
    private Sound dieSound;
    private Sound eatSound;

    public Player(AssetManager assetManager) {
        hurtSound = assetManager.get("sounds/hurt.wav");
        dieSound = assetManager.get("sounds/death.wav");
        eatSound = assetManager.get("sounds/eat.wav");
        pos = new Vector2(0f, 0f);
        velocity = new Vector2(0f, 0f);
        collisionRect = new Rectangle(0f, 0f, GameParams.PERSON_SIZE, GameParams.PERSON_SIZE);
        super.speed = GameParams.PLAYER_SPEED;
        loadAnimations(assetManager);
    }

    @Override
    public void update(float delta) {

        super.animationTimer += delta;

        Vector2 scaledVelocity = velocity.scl(delta);
        setPos(pos.x + scaledVelocity.x, pos.y + scaledVelocity.y);
    }

    @Override
    protected void loadAnimations(AssetManager assetManager) {
        TextureAtlas atlas = assetManager.get("player/player.atlas");
        super.leftWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("player_walk_left"), Animation.PlayMode.LOOP);
        super.rightWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("player_walk_right"), Animation.PlayMode.LOOP);
        super.upWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("player_walk_up"), Animation.PlayMode.LOOP);
        super.downWalkAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("player_walk_down"), Animation.PlayMode.LOOP);
        super.downAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("player_eat"));
        animsToFinish.add(downAttackAnim);
        super.leftAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("player_eat_left"));
        animsToFinish.add(leftAttackAnim);
        super.rightAttackAnim = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegions("player_eat_right"));
        animsToFinish.add(rightAttackAnim);

        // death animation is a little longer. it's kind of important so whatever
        deathAnimation = new Animation<TextureRegion>(GameParams.SECONDS_PER_LOOPING_ANIM_FRAME, atlas.findRegions("player_death"));
        animsToFinish.add(deathAnimation);

        hitUp = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegion("player_hurt_up"));
        animsToFinish.add(hitUp);
        hitDown = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegion("player_hurt_down"));
        animsToFinish.add(hitDown);
        hitRight = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegion("player_hurt_right"));
        animsToFinish.add(hitRight);
        hitLeft = new Animation<TextureRegion>(GameParams.SECONDS_PER_NORMAL_ANIM_FRAME, atlas.findRegion("player_hurt_left"));
        animsToFinish.add(hitLeft);

        super.setCurrentAnimation(downWalkAnim);
    }

    @Override
    public void drawDebug(ShapeRenderer shapeRenderer) {
        Color origColor = new Color(shapeRenderer.getColor());
        shapeRenderer.setColor(Color.GREEN);
        super.drawDebug(shapeRenderer);
        shapeRenderer.setColor(origColor);
    }

    public void hit() {
        hurtSound.play();
        switch (dir) {
            case UP:
            case UP_LEFT:
            case UP_RIGHT:
                setCurrentAnimation(hitUp);
                break;
            case NONE:
            case DOWN:
            case DOWN_LEFT:
            case DOWN_RIGHT:
                setCurrentAnimation(hitDown);
                break;
            case RIGHT:
                setCurrentAnimation(hitRight);
                break;
            case LEFT:
                setCurrentAnimation(hitLeft);
                break;
        }
    }

    public void eat() {
        eatSound.play();
        switch (dir) {
            case UP:
            case UP_LEFT:
            case UP_RIGHT:
                break;
            case DOWN:
            case DOWN_LEFT:
            case DOWN_RIGHT:
                setCurrentAnimation(downAttackAnim);
                break;
            case RIGHT:
                setCurrentAnimation(rightAttackAnim);
                break;
            case LEFT:
                setCurrentAnimation(leftAttackAnim);
                break;
        }
    }

    public void die() {
        dieSound.play();
        // don't let this method return without the death animation being set
        do {
            setCurrentAnimation(deathAnimation);
            // if we don't increment the animation timer, the current animation will never finish
            animationTimer += Gdx.graphics.getDeltaTime();
        } while (super.getCurrentAnimation() != deathAnimation);
    }

    public boolean finishedDying() {
        boolean isDone = getCurrentAnimation() == deathAnimation
                && getCurrentAnimation().isAnimationFinished(animationTimer);
        if (isDone) {
            dieSound.stop();
        }

        return isDone;
    }
}
