package com.gitlab.sorou.foodsurvivalgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gitlab.sorou.foodsurvivalgame.FoodSurvivalGame;
import com.gitlab.sorou.foodsurvivalgame.GameParams;

public class GameOverScreen extends ScreenAdapter {
    FoodSurvivalGame game;
    private BitmapFont font = new BitmapFont();
    private GlyphLayout glyphLayout = new GlyphLayout();
    private SpriteBatch batch = new SpriteBatch();

    private OrthographicCamera camera;
    private Viewport viewport;

    private Texture background;
    private int score;

    private float waitTimer = 0.5f;

    public GameOverScreen(FoodSurvivalGame game, int finalScore) {
        this.game = game;
        this.score = finalScore;
    }

    @Override
    public void show() {
        background = game.assetManager.get("bg/bg.png");
        camera = new OrthographicCamera();
        camera.position.set(GameParams.GAME_WIDTH / 2, GameParams.GAME_HEIGHT / 2, 0);
        camera.update();
        viewport = new FitViewport(GameParams.GAME_WIDTH, GameParams.GAME_HEIGHT, camera);
    }

    @Override
    public void hide() {
        this.dispose();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);
        waitTimer -= delta;
        if (waitTimer <= 0) {
            if (Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
                game.setScreen(new StartScreen(game));
                return;
            }
        }

        batch.begin();
        draw();
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        font.dispose();
        batch.dispose();
        super.dispose();
    }

    private void draw() {
        batch.draw(background, 0f, 0f, viewport.getScreenWidth(), viewport.getScreenHeight());

        String gameOverText = "Game over! Press any key to try again!\nFinal score: " + score;
        glyphLayout.setText(font, gameOverText);

        float textX = 0f;
        float textY = (viewport.getWorldHeight() + glyphLayout.height) / 2;

        font.draw(batch, gameOverText, textX, textY, viewport.getWorldWidth(), Align.center, true);
    }
}
