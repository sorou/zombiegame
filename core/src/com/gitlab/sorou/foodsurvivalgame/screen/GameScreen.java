package com.gitlab.sorou.foodsurvivalgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gitlab.sorou.foodsurvivalgame.Direction;
import com.gitlab.sorou.foodsurvivalgame.Enemy;
import com.gitlab.sorou.foodsurvivalgame.FoodSurvivalGame;
import com.gitlab.sorou.foodsurvivalgame.GameParams;
import com.gitlab.sorou.foodsurvivalgame.Person;
import com.gitlab.sorou.foodsurvivalgame.Player;
import com.gitlab.sorou.foodsurvivalgame.bullet.Banana;
import com.gitlab.sorou.foodsurvivalgame.bullet.Bullet;
import com.gitlab.sorou.foodsurvivalgame.bullet.Watermelon;

import static com.gitlab.sorou.foodsurvivalgame.Direction.randomDir;

public class GameScreen extends ScreenAdapter {

    private FoodSurvivalGame game;

    private ShapeRenderer shapeRenderer;
    private SpriteBatch batch;

    private Camera camera;
    private Viewport viewport;

    private Player player;
    private Array<Enemy> enemies;
    private Array<Bullet> bullets;

    private BitmapFont bitmapFont;
    private GlyphLayout glyphLayout;

    private Texture background;

    private float spawnTimer = 0;
    private int score = 0;

    private boolean playerDead = false;

    public GameScreen(FoodSurvivalGame game) {
        this.game = game;
    }

    @Override
    public void show() {
        background = game.assetManager.get("bg/bg.png");
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        camera.position.set(GameParams.GAME_WIDTH / 2, GameParams.GAME_HEIGHT / 2, 0f);
        camera.update();

        viewport = new FitViewport(GameParams.GAME_WIDTH, GameParams.GAME_HEIGHT, camera);

        bitmapFont = game.assetManager.get("text.fnt");
        glyphLayout = new GlyphLayout();

        player = new Player(game.assetManager);
        player.isActive = true;
        player.setPos(GameParams.GAME_WIDTH / 2, GameParams.GAME_HEIGHT / 2);

        enemies = new Array<>();
        bullets = new Array<>();
        spawnEnemy();
        spawnTimer = GameParams.ENEMY_SPAWN_SECONDS;
    }

    @Override
    public void render(float delta) {
        clearScreen(Color.BLACK);

        spawnTimer -= delta;

        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);

        if (!playerDead) {
            if (spawnTimer <= 0 && enemies.size < 10) {
                spawnEnemy();
                spawnTimer = GameParams.ENEMY_SPAWN_SECONDS;
            }

            handleInput();
            for (Enemy enemy : enemies) {
                enemy.update(delta);
                if (bullets.size < 10 && enemy.shotTimer <= 0) {
                    bullets.add(enemy.shoot(player));
                }
            }

            for (Bullet bullet : bullets) {
                bullet.update(delta);
                if (bullet.collisionCircle.x < 0 || bullet.collisionCircle.x > GameParams.GAME_WIDTH) {
                    bullets.removeValue(bullet, true);
                }
                if (bullet.collisionCircle.y < 0 || bullet.collisionCircle.y > GameParams.GAME_HEIGHT) {
                    bullets.removeValue(bullet, true);
                }
            }

            handleCollision();
        } else {
            if (player.finishedDying()) {
                gameOver();
                return;
            }
        }

        player.update(delta);

        batch.begin();
        draw();
        batch.end();

//        drawDebug();

    }

    @Override
    public void dispose() {
        batch.dispose();
        shapeRenderer.dispose();
        bitmapFont.dispose();
        super.dispose();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void hide() {
        this.dispose();
    }

    private void draw() {
        batch.draw(background, 0f, 0f, viewport.getScreenWidth(), viewport.getScreenHeight());
        player.draw(batch);

        for (Enemy enemy : enemies) {
            enemy.draw(batch);
        }

        for (Bullet bullet : bullets) {
            bullet.draw(batch);
        }

        bitmapFont.getData().setScale(0.4f);
        drawScore(batch);
        drawHealth(batch);
    }

    private void drawDebug() {
        shapeRenderer.end();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        player.drawDebug(shapeRenderer);
        for (Enemy enemy : enemies) {
            enemy.drawDebug(shapeRenderer);
        }

        for (Bullet bullet : bullets) {
            bullet.drawDebug(shapeRenderer);
        }

        shapeRenderer.end();

    }

    private void drawScore(SpriteBatch batch) {
        String scoreTxt = "Score: " + score;
        glyphLayout.setText(bitmapFont, scoreTxt);
        bitmapFont.draw(batch, scoreTxt, viewport.getWorldWidth() - glyphLayout.width - 10,
                viewport.getWorldHeight() - glyphLayout.height);
    }

    private void drawHealth(SpriteBatch batch) {
//        shapeRenderer.end();
//        shapeRenderer.setColor(Color.RED);
//        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//        shapeRenderer.rect(0f, viewport.getWorldHeight() - GameParams.HEALTH_BAR_HEIGHT - 5,
//                GameParams.HEALTH_BAR_WIDTH * (player.health / 100f), GameParams.HEALTH_BAR_HEIGHT);
//        shapeRenderer.end();
//        shapeRenderer.setColor(Color.WHITE);

        String healthTxt = "Health: " + player.health;
        glyphLayout.setText(bitmapFont, healthTxt);
        bitmapFont.draw(batch, healthTxt, 10f, viewport.getWorldHeight() - glyphLayout.height);
    }

    private void handleInput() {
        boolean left = Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT);
        boolean right = Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT);
        boolean up = Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP);
        boolean down = Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN);

        if (left && up) {
            player.move(Direction.UP_LEFT);
        } else if (left && down) {
            player.move(Direction.DOWN_LEFT);
        } else if (right && up) {
            player.move(Direction.UP_RIGHT);
        } else if (right && down) {
            player.move(Direction.DOWN_RIGHT);
        } else if (left) {
            player.move(Direction.LEFT);
        } else if (right) {
            player.move(Direction.RIGHT);
        } else if (up) {
            player.move(Direction.UP);
        } else if (down) {
            player.move(Direction.DOWN);
        } else {
            player.stop();
        }
    }

    private void clearScreen(Color color) {
        Gdx.gl.glClearColor(color.r, color.g, color.b, color.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void spawnEnemy() {
        Vector2 pos = new Vector2();
        switch (randomDir()) {
            case LEFT:
                pos.x = 0;
                pos.y = MathUtils.random(0f, GameParams.GAME_HEIGHT - GameParams.PERSON_SIZE);
                break;
            case RIGHT:
                pos.x = GameParams.GAME_WIDTH - GameParams.PERSON_SIZE;
                pos.y = MathUtils.random(0f, GameParams.GAME_HEIGHT - GameParams.PERSON_SIZE);
                break;
            case UP:
                pos.x = MathUtils.random(0f, GameParams.GAME_WIDTH - GameParams.PERSON_SIZE);
                pos.y = GameParams.GAME_HEIGHT - GameParams.PERSON_SIZE;
                break;
            case DOWN:
                pos.x = MathUtils.random(0f, GameParams.GAME_WIDTH - GameParams.PERSON_SIZE);
                pos.y = 0;
                break;
        }

        Enemy newEnemy = new Enemy(pos, game.assetManager);
        newEnemy.isActive = true;
        this.enemies.add(newEnemy);
    }

    private void handleCollision() {
        for (Enemy enemy : enemies) {
            if (player.collisionRect.overlaps(enemy.collisionRect)) {
                score += GameParams.POINTS_PER_ENEMY;
                if (enemies.size == 10) {
                    spawnTimer = GameParams.ENEMY_SPAWN_SECONDS;
                }
                player.health = Math.min(100, player.health + GameParams.POINTS_PER_ENEMY);
                enemy.isActive = false;
                enemies.removeValue(enemy, true);
                player.eat();
            }
        }

        for (Bullet bullet : bullets) {
            if (Intersector.overlaps(bullet.collisionCircle, player.collisionRect)) {
                // prevent player health from going negative
                player.health = Math.max(0, player.health - (int) bullet.damage);
                bullets.removeValue(bullet, true);
                player.hit();
                if (checkDeath()) {
                    return;
                }
            }

            // remove banana once it goes back to its target
            if (bullet instanceof Banana) {
                Person bananaSource = ((Banana) bullet).source;
                if (bananaSource.isActive) {
                    if (((Banana) bullet).isReturning
                            && Intersector.overlaps(bullet.collisionCircle, bananaSource.collisionRect)) {
                        bullets.removeValue(bullet, true);
                    }
                }
            }

            if (bullet instanceof Watermelon) {
                if (((Watermelon) bullet).explosionFinished) {
                    bullets.removeValue(bullet, true);
                }
            }
        }
    }

    private boolean checkDeath() {
        if (player.health <= 0) {
            playerDead = true;
            for (int i = 0; i < enemies.size; i++) {
                enemies.get(i).stop();
            }

            for (int i = 0; i < bullets.size; i++) {
                bullets.get(i).stop();
            }

            player.die();
        }

        return playerDead;
    }

    private void gameOver() {
        this.game.setScreen(new GameOverScreen(this.game, score));
    }
}
