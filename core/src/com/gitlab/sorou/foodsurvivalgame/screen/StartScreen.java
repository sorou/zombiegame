package com.gitlab.sorou.foodsurvivalgame.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gitlab.sorou.foodsurvivalgame.FoodSurvivalGame;
import com.gitlab.sorou.foodsurvivalgame.GameParams;

public class StartScreen extends ScreenAdapter {

    private FoodSurvivalGame game;
    private BitmapFont font = new BitmapFont();
    private GlyphLayout glyphLayout = new GlyphLayout();
    private SpriteBatch batch = new SpriteBatch();
    private OrthographicCamera camera;
    private Viewport viewport;
    private final String title = "Food Survival!";

    private Music titleSong;
    private Texture background;
    private float waitTimer = 0.5f;

    public StartScreen(FoodSurvivalGame game) {
        this.game = game;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera();
        camera.position.set(GameParams.GAME_WIDTH / 2, GameParams.GAME_HEIGHT / 2, 0);
        camera.update();
        viewport = new FitViewport(GameParams.GAME_WIDTH, GameParams.GAME_HEIGHT, camera);

        game.assetManager.load("text.fnt", BitmapFont.class);
        game.assetManager.finishLoadingAsset("text.fnt");
        font = game.assetManager.get("text.fnt");
        game.assetManager.load("enemies/gmachine/gumball_machine.atlas", TextureAtlas.class);
        game.assetManager.load("enemies/bbunch/banana_bunch.atlas", TextureAtlas.class);
        game.assetManager.load("enemies/watermelon/watermelon.atlas", TextureAtlas.class);
        game.assetManager.load("player/player.atlas", TextureAtlas.class);
        game.assetManager.load("objects/gumball.atlas", TextureAtlas.class);
        game.assetManager.load("objects/watermelon_explosion.atlas", TextureAtlas.class);
        game.assetManager.load("objects/banana.atlas", TextureAtlas.class);
        game.assetManager.load("sounds/boom.wav", Sound.class);
        game.assetManager.load("sounds/death.wav", Sound.class);
        game.assetManager.load("sounds/eat.wav", Sound.class);
        game.assetManager.load("sounds/hurt.wav", Sound.class);
        game.assetManager.load("sounds/shoot.wav", Sound.class);
        game.assetManager.load("bg/bg.png", Texture.class);
        background = game.assetManager.finishLoadingAsset("bg/bg.png");

        game.assetManager.load("sounds/title.mp3", Music.class);
        titleSong = game.assetManager.finishLoadingAsset("sounds/title.mp3");
        titleSong.setLooping(true);
        titleSong.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        waitTimer -= delta;
        if (waitTimer <= 0) {
            if (Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
                startGame();
                return;
            }
        }

        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);

        batch.begin();
        batch.draw(background, 0f, 0f);
        glyphLayout.setText(font, title);
        float titleX = 0;
        float titleY = (viewport.getWorldHeight() - glyphLayout.height);
        font.draw(batch, title, titleX, titleY, viewport.getWorldWidth(), Align.center, true);

        font.getData().setScale(0.6f);
        String playText = "Food has taken over the world!\n\nUse arrow keys/WASD to eat as many as you can!\nHow long can you survive the food menace?";
        float playX = 0f;
        float playY = (viewport.getWorldHeight() - glyphLayout.height) / 2;
        glyphLayout.setText(font, playText);
        font.draw(batch, playText, playX, playY, viewport.getWorldWidth(), Align.center, true);
        font.getData().setScale(1f);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void hide() {
        this.dispose();
    }

    @Override
    public void dispose() {
        font.dispose();
        batch.dispose();
        titleSong.dispose();
        super.dispose();
    }

    private void startGame() {
        game.assetManager.finishLoading();
        titleSong.stop();
        game.setScreen(new GameScreen(game));
    }
}
