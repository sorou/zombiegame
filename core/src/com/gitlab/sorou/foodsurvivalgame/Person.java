package com.gitlab.sorou.foodsurvivalgame;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.HashSet;
import java.util.Set;

public abstract class Person {
    Vector2 pos;
    Vector2 velocity;

    Direction dir = Direction.NONE;

    public boolean isActive;

    float speed;

    public Rectangle collisionRect;

    protected Animation<TextureRegion> downWalkAnim;
    protected Animation<TextureRegion> upWalkAnim;
    protected Animation<TextureRegion> leftWalkAnim;
    protected Animation<TextureRegion> rightWalkAnim;

    // animations that should finish before we start another one
    protected Set<Animation<TextureRegion>> animsToFinish = new HashSet<>();
    protected Animation<TextureRegion> downAttackAnim;
    protected Animation<TextureRegion> leftAttackAnim;
    protected Animation<TextureRegion> rightAttackAnim;

    private Animation<TextureRegion> currentAnimation;
    // reset whenever the animation changes
    protected float animationTimer = 0f;

    public abstract void update(float delta);

    protected abstract void loadAnimations(AssetManager assetManager);

    public void draw(SpriteBatch batch) {
        batch.draw(currentAnimation.getKeyFrame(animationTimer),
                pos.x, pos.y,
                collisionRect.width, collisionRect.height);
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.rect(collisionRect.x, collisionRect.y,
                collisionRect.width, collisionRect.height);
    }

    public void setPos(float x, float y) {
        pos.x = MathUtils.clamp(x, 0f, GameParams.GAME_WIDTH - GameParams.PERSON_SIZE);
        pos.y = MathUtils.clamp(y, 0f, GameParams.GAME_HEIGHT - GameParams.PERSON_SIZE);

        collisionRect.x = pos.x;
        collisionRect.y = pos.y;
    }

    public Vector2 getPos() {
        return this.pos;
    }

    public void move(Direction dir) {
        this.dir = dir;
        switch (this.dir) {
            case LEFT:
                velocity.set(-speed, 0);
                setCurrentAnimation(leftWalkAnim);
                break;
            case RIGHT:
                velocity.set(speed, 0);
                setCurrentAnimation(rightWalkAnim);
                break;
            case UP:
                velocity.set(0, speed);
                setCurrentAnimation(upWalkAnim);
                break;
            case UP_LEFT:
                // if we just add speed to the x and y components we end up with a vector w/magnitude
                // over twice the intended speed.
                scaleVecToMagnitude(velocity.set(-speed, speed), speed);
                setCurrentAnimation(upWalkAnim);
                break;
            case UP_RIGHT:
                scaleVecToMagnitude(velocity.set(speed, speed), speed);
                setCurrentAnimation(upWalkAnim);
                break;
            case DOWN:
                velocity.set(0, -speed);
                setCurrentAnimation(downWalkAnim);
                break;
            case DOWN_LEFT:
                scaleVecToMagnitude(velocity.set(-speed, -speed), speed);
                setCurrentAnimation(downWalkAnim);
                break;
            case DOWN_RIGHT:
                scaleVecToMagnitude(velocity.set(speed, -speed), speed);
                setCurrentAnimation(downWalkAnim);
                break;
            default:
                velocity.set(Vector2.Zero);
                setCurrentAnimation(downWalkAnim);
        }
    }

    public void stop() {
        move(Direction.NONE);
    }

    protected Animation<TextureRegion> getCurrentAnimation() {
        return currentAnimation;
    }

    // change the current animation to nextAnim unless the current animation is one that
    // needs to finish and it hasn't yet.
    protected void setCurrentAnimation(Animation<TextureRegion> nextAnim) {
        setCurrentAnimation(nextAnim, false);
    }

    // same as setCurrentAnimation but uses restartIfCurrent to determine what to do if nextAnim == currentAnimation
    // if restartIfCurrent is true, the current animation is reset to its first frame
    // otherwise let it continue
    protected void setCurrentAnimation(Animation<TextureRegion> nextAnim, boolean restartIfCurrent) {
        if (!restartIfCurrent && currentAnimation == nextAnim) {
            return;
        }

        if (!animsToFinish.contains(currentAnimation)
                || currentAnimation.isAnimationFinished(animationTimer)) {
            currentAnimation = nextAnim;
            animationTimer = 0f;
        }
    }

    private void scaleVecToMagnitude(Vector2 vec, float magnitude) {
        vec.limit(1).scl(magnitude);
    }
}
