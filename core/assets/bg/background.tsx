<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="background" tilewidth="32" tileheight="32" tilecount="4" columns="4">
 <image source="background.png" width="128" height="32"/>
 <terraintypes>
  <terrain name="map" tile="0"/>
 </terraintypes>
 <tile id="0" terrain="0,0,0,0" probability="0.083"/>
 <tile id="1" terrain="0,0,0,0" probability="0.083"/>
 <tile id="2" terrain="0,0,0,0" probability="0.75"/>
 <tile id="3" terrain="0,0,0,0" probability="0.083"/>
</tileset>
