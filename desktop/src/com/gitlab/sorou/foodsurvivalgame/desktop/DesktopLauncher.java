package com.gitlab.sorou.foodsurvivalgame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gitlab.sorou.foodsurvivalgame.GameParams;
import com.gitlab.sorou.foodsurvivalgame.FoodSurvivalGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = (int) GameParams.GAME_WIDTH;
		config.height = (int) GameParams.GAME_HEIGHT;
		new LwjglApplication(new FoodSurvivalGame(), config);
	}
}
